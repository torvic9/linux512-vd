# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

# Maintainer (vd): torvic9

pkgbase=linux512-vd
pkgname=('linux512-vd' 'linux512-vd-headers')
_basekernel=5.12
_kernelname=-vd
_sub=16
#_rc=rc8
pkgver=${_basekernel}.${_sub}
pkgrel=1
_archpatch=20210616
_stablequeue=b1cd1baa19
arch=('x86_64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc'
            'python' 'elfutils' 'git' 'libelf')
options=('!strip')
source=(https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-${pkgver}.tar.{xz,sign}
    #https://git.kernel.org/torvalds/t/linux-${_basekernel}-${_rc}.tar.gz
    # the main kernel config files
    'config.x86_64' 'config.x270' 'config.zen2' 'x509.genkey' "${pkgbase}.preset"
    #
    # Prepatch from stable-queue
    # "prepatch-${_basekernel/./}-g${_stablequeue}.patch"
    #
    # Arch patches
    0001-arch-patches512-${_archpatch}.patch::https://raw.githubusercontent.com/sirlucjan/kernel-patches/master/5.12/arch-patches-v7/0001-arch-patches.patch
    # CPU patches
    0002-graysky-cpu-optimizations-gcc10.patch
    0003-enable-O3-for-all-archs-and-add-option-for-O1.patch
    # Clear Linux
    0004-clearlinux-tweak-intel-cpuidle.patch
    0005-clearlinux-add-config-opt-for-raid6-bench.patch
    # AMD enhancements
    0006-dma-add-support-for-amd-ptdma-controller-driver-v8.patch
    0007-acpi-add-processor-to-the-ignore-PSD-override-list.patch
    # bfq fixes
    0008-block-bfq-fixes-and-improvements.patch
    # rcu fixes
    0009-rcu-fixes-next.patch
    # tip:sched/core: 0010
    # amdgpu
    0011-drm-amdgpu-fixes-next.patch
    # lru fix
    0012-mm-replace-migrate_prep-with-lru_add_drain_all-v3.patch
    # zstd 1.4.10
    0013-zstd-1410-v11.patch
    # amd_energy fixes from next
    0014-hwmon-amd-energy-next.patch
    # Nuvoton nc677x driver
    0015-i2c-nuvoton-nc677x-hwmon-driver-git.patch::https://gitlab.com/CalcProgrammer1/OpenRGB/-/raw/master/OpenRGB.patch
    # async initramfs unpacking
    0016-init-initramfs.c-allow-asynchronous-unpacking-v3.patch
    # btrfs patches
    0017-btrfs-fixes-next.patch
    # various sched fixes: 0018
    # igb fix
    0019-igb-fix-netpoll-exit-with-traffic.patch
    #
    # additional patchsets
    # futex_wait_multiple
    1001-futex-512-sirlucjan.patch::https://raw.githubusercontent.com/sirlucjan/kernel-patches/master/5.12/futex-patches-v2/0001-futex-resync-from-gitlab.collabora.com.patch
    # spf anon - disabled, needs update for 5.12.10
    # 1002-mm-spf-anon.patch
    # new lrng
    1003-dev-random-new-lrng-approach-v40.patch
    1004-lrng-update-20210623.patch
    #
    # MANJARO Patches
    #
    # vd/hho patches
    2001-add-acpi_call-module.patch
    2002-tune-vm-mm-and-vfs-settings.patch
    2003-tune-cfs-settings.patch
    2004-allow-cpufreq-ondemand-with-muqss-and-projectc.patch
    2005-tune-cpufreq-ondemand-settings.patch
    2006-zstd-add-module-compression.patch
    2007-allow-setting-zstd-compression-level-for-kernel.patch
    2008-allow-setting-zstd-compression-level-for-modules.patch
    2009-optimize-lz4-gzip-and-xz-kernel-compression-options.patch
    # ntfs3 driver
    2010-ntfs-rw-gpl-driver-implementation-by-paragon-v26.patch
    2011-ntfs3-make-ntfs3-compile-with-clang-12.patch
    # objtool crypto fixes from jpoimboeuf
    2012-objtool-crypto-jp.patch
    # speed improvement with trim_unused_ksyms
    2013-kbuild-build-speed-improvment-of-trim_unused_ksyms.patch
    # choose clang thinlto cachedir
    2014-kbuild-add-option-for-thinlto-cache-dir.patch
    #
    # Project C (BMQ+PDS): 300x
    #
    # reverts
    # 8001-sched-fair-update_pick_idlest.revert  # possible regression
    #
    # clang pgo support, experimental
    # 9001-clang-pgo-v9.patch
    #
    # pgo profile data
    # vmlinux.profdata
    #
)

validpgpkeys=(
  'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
  '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
)

sha256sums=('1aad8e29610f1401f5970e1bcf87062ac4a93529442340308502c5305190f6fe'
            'SKIP'
            '37744de7d12b57f4161a102c1bdf27cb67ca573bc5d05ab3e147b17bac116ebf'
            '2c4f452d2a66a6d64b0ce460830f6de93e36c255f63f41bf8a2b29830b16d847'
            '80a32e564653631c16cab52593354044648bbbaca577b31a705215f8ab1763c2'
            'ab010dc5ef6ce85d352956e5996d242246ecd0912b30f0b72025c38eadff8cd5'
            '77442a85dc4b94534414a5e1055964d6d9b20b2028fd9ee107a4dce760c4fbac'
            'dd89db0483301a62cef23ba797fee1fb9804f570f952df87fe418ee22e353227'
            '9083b94bf9f547cceeed9fe2f37fb201e42d5b00734a86e4ea528447a59d4b9a'
            '3d38fc4052b999b67aaed9fe9a4ba6ffd778ffbf7e94a66d5577391dbd08d12a'
            'b78ab97a629579ebc27ff175eacc9162a07c9b925cebd91099c97ef509bd117d'
            'b817e7da8f4901cf2dda0f2fe7b9d8243f32a42d5729e953521ef18eec7a8eb9'
            '4ff1bb0db25a3c2e28ef08c2917c4b22d9ba380e76cfc8b85e206af87ac90278'
            '5000348583882523ef3c36df27eabf4355e83d0605081a3bf5d4aaa28e518162'
            '43121f24e7453c55b125cde6fdda073f917d86ce379e876d7f270e9b055fd23e'
            '6996dfd97fdc354c5f1182017956cdb4ac523b94cece362a5752efc4b2c30ebc'
            '9d499a6d815423987853634fb3906a4a2afc1ac06714e450bc2eade85760d106'
            '82e4061564bfd0c0cae9ed065c17d252b1617be76f836b1f6107fe4795e7f19e'
            '6b653ce9b48c0d890b5b4762a6846a5a7f7d60a8b9fb3cadf76de0357ea61461'
            '143e505e05863e7112f7a17d75508cab867585e7a59143d4c343ac13b901fcd1'
            'e7d724ac15daf428aa1e6a03737e5c1d040892d55fda8a66897fcac9323f285c'
            'aab891ea641073e8e0fc2632f862411d6ba14a7b500dc791626ccf6454e4c68d'
            '6f910fbc2c600d5469ffba854d07304b1b021b0ed470bedfb17bbf94b8e921b0'
            '6eb8ce7f31b87a6caa7fded2799d4d09d4a207d7ababff7eae62fae2dd59b0ef'
            '4ff5c33785445f103d479196bb33c4f991d6c5f56df5c643ec6d2451368f0605'
            'd63575dcb863b9f98081f04f1c0ef75ce78db20f07138f38b2cf8b3b18088183'
            '4e330feaee4b3e9ab19bcc24d6215093bdc325043bb00ad746deef59b2498de7'
            'fc00c166c0090dea30b628afb431ffe225f1917ea38220d3df5b05de69c7052e'
            'f7a36231b794022d49e53f464d25e48f2eebf6266c2cbe5756c63aa3bf03bae7'
            'acca50a9ffee480f29bd7de6e8b5963dc0d37d3103871d75bcffdb2acce6c82d'
            '4c0beb1f181e7ee22e978f447aaccc3bd7f326e861a5afb5798922b1e7efc2ec'
            '02d2c0e6b2459d4dbd6d4cecb3b269545a78b86cc9d2d3a0fda80bb3c3ee7604'
            'c4f83f4422b0456c53bbe2556a7dfbf6433094b953f8be124f7371a768167efb'
            '4887e85aec6e6ec903cc50ce2e4a802f7110ee5062ed6dbf8935e21cf8dada57'
            '57f2b6f06c9c498131753e4fca2884c32e12b1acbd1ba274408ad4a8fc9595c7'
            'fd0b4887aa20769b524ddba498c6f2b31d290f6d4b40bee4e519e4c268ebf136'
            '3d280f334c9de8d760fe477980274f22af4c5b739cd6f16d9c1b6044e08870cd'
            'aa799285ecb68f22ab825913c36441a46b40367eae53da0005882d68ec6d9f84'
            'fb861c9fef360458634d05efed08c1554f65d51ab2767a8a68bcaeab7e8e7244'
            'c6a5e2301e80577d5e6b9821de28a4f8e2db854ea9e65fa28ad9508991b60424'
            'e6a12bfdf8bb98e1d927a856b44174a8018fdbe950ec10d80b4477397d8e764a')

export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_HOST=eos
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

# signing
_signing=0
# edit the paths below to point to your signing keys
_key="$HOME/build/keys/vd512-kernel-key.pem"
_pubkey="$HOME/build/keys/vd512-kernel-pubkey.pem"

# custom clang path
# export PATH=/opt/clang12/bin:$PATH
_clang=0

if [[ ${_clang} -eq 1 ]]; then
	LLVMOPTS="LLVM=1 LLVM_IAS=1"
	CLANGOPTS="CC=clang LD=ld.lld"
else
	LLVMOPTS=""
	CLANGOPTS=""
fi

# Schedulers: cfs,bmq,pds
_sched="cfs"

if [[ ${_sched} == "cfs" ]]; then
    source+=('0010-tip-sched-core-20210421.patch'
            '0018-sched-fair-various-fixes.patch')
    sha256sums+=('95c62dc252f0566d38cce4bb2334786f41548e6c5211b3ed76b0915c62e2eb8b'
                'b3188e03cf3fb083e6862281dd68b847784d1ba6865fc0427fb80732ab26cc4d')
elif [[ ${_sched} == "bmq" || ${_sched} == "pds" ]]; then
    source+=('0010-tip-sched-core-prjc-20210421.patch'
             '3001-projectc512-r0-vd.patch'
             '3002-projectc512-r1-vd.patch'
             '3003-projectc512-fix-compilation-error.patch')
    sha256sums+=('685462deced65bf57dd98d8337a711daaac3f4c8843d4327ff16cf7f48e09356'
                'c714b059061d46c03bbe5f3d07845e192e5e46c379542604806c4612767fae2d'
                '7f1bdc3d3d9edb5cfafe974f9a281ac55d09fe119b13cc311bd71dacb66d1640'
                '9f48c5893ed00c40a2e0abde22e221244f4e64e194a31be7ceeefe2573395b9a')
else
	echo "Schedulers must be one of cfs, bmq or pds. Aborting." && exit 2
fi

TB=$(tput bold)
TN=$(tput sgr0)

prepare() {

  cd "${srcdir}/linux-${pkgver}"

  echo "-${_kernelname/-/}" > localversion.10-pkgname
  echo "-${pkgrel}" > localversion.20-pkgrel

  echo -e "\n${TB}* APPLYING PATCHES${TN}"

  #echo -e "\n---- Reverts:" # add reverts here
  #patch -Rp1 -i "../8001-sched-fair-update_pick_idlest.revert"

  # apply patch from the source array (should be a pacman feature)
  local filename filename2
  for filename in "${source[@]}"; do
  	if [[ "$filename" =~ \.patch$ || "$filename" =~ \.diff$ ]]; then
		filename="${filename%%::*}"
		filename2="${filename#*-}"
        	echo -e "\n---- Applying patch ${TB}${filename2%%.*}${TN}:"
        	patch -Np1 -i "../${filename}"
  	fi
  done

  # kernel config
  echo -e "\n${TB}* KERNEL CONFIGURATION${TN}"
  local _config
  echo "---- Select configuration file:"
  echo "${TB}1)${TN} Default"
  echo "${TB}2)${TN} Zen2"
  echo "${TB}3)${TN} X270"
  while true ; do
  	read -p "Enter number (1-3): " _config
	  case ${_config} in
		1) cat ../config.x86_64 > ./.config && break ;;
		2) cat ../config.zen2 > ./.config && break ;;
		3) cat ../config.x270 > ./.config && break ;;
		*) echo "Please enter a number (1-3)!" && continue ;;
  	  esac
  done

  if [[ ${_signing} -eq 1 ]] ; then
    cat ${_key} > ./certs/vd512-kernel-key.pem
    cat ${_pubkey} > ./certs/vd512-kernel-pubkey.pem
    sed -i "s|signing_key|vd512-kernel-key|" ./.config
    sed -ri "s|^(CONFIG_SYSTEM_TRUSTED_KEYS=).*|\1\"certs/vd512-kernel-pubkey.pem\"|" ./.config
  else
    cat ../x509.genkey > ./certs/x509.genkey
  fi

  #make $LLVMOPTS prepare
  make $LLVMOPTS olddefconfig
  if [[ ${_sched} == "bmq" || ${_sched} == "pds" ]]; then
    ./scripts/config -e CONFIG_SCHED_ALT
    [[ ${_config} -eq 2 ]] && ./scripts/config -d CONFIG_CPU_FREQ_DEFAULT_GOV_SCHEDUTIL && \
    	./scripts/config -e CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND
    [[ ${_sched} == "bmq" ]] && ./scripts/config -e CONFIG_SCHED_BMQ
    [[ ${_sched} == "pds" ]] && ./scripts/config -e CONFIG_SCHED_PDS
  fi
  # get kernel version
  make $LLVMOPTS -s kernelrelease > version
  printf "\n  Prepared %s version %s\n" "$pkgbase" "$(<version)"
  read -p "---- Enter 'y' for nconfig: " NCONFIG
  [[ $NCONFIG == "y" ]] && make $LLVMOPTS nconfig

  # rewrite configuration
  yes '' | make $LLVMOPTS config >/dev/null
}

build() {
  cd "${srcdir}/linux-${pkgver}"

  # copy pgo profile data
  # cp $srcdir/vmlinux.profdata ./

  # build!
  make $LLVMOPTS LOCALVERSION= bzImage modules
  # below cmd is for using a pgo profile, 1st without LTO, 2nd with LTO
  # make $LLVMOPTS KCFLAGS=-fprofile-use=vmlinux.profdata LOCALVERSION= bzImage modules
  # make $LLVMOPTS KCFLAGS=-lto-cs-profile-file=vmlinux.profdata LOCALVERSION= bzImage modules

  # build turbostat
  # make $CLANGOPTS -C tools/power/x86/turbostat
}

package_linux512-vd() {
  pkgdesc="The ${pkgbase/linux/Linux} vd kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=27')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=(VIRTUALBOX-GUEST-MODULES)
  replaces=(linux512-vd-virtualbox-guest-modules)

  cd "${srcdir}/linux-${pkgver}"

  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make $CLANGOPTS -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "${pkgbase}" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  # make room for external modules
  local _extramodules="extramodules-${pkgbase}"
  ln -s "../${_extramodules}" "$modulesdir/extramodules"
  # add real version for building modules and running depmod from hook
  echo "${kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  echo -e "\n${TB}* INSTALLING MODULES${TN}"
  make $CLANGOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" INSTALL_MOD_STRIP=1 modules_install
  # do not strip when building a pgo kernel
  # make $CLANGOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" modules_install

  # remove build and source links
  rm $modulesdir/source
  rm $modulesdir/build
  [[ -f ./certs/vd512-kernel-key.pem ]] && rm ./certs/vd512-kernel-key.pem

  # add mkinitcpio preset (not strictly needed)
  install -Dm644 "$srcdir/${pkgbase}.preset" "$pkgdir/etc/mkinitcpio.d/${pkgbase}.preset"
}

package_linux512-vd-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} vd kernel"

  cd "${srcdir}/linux-${pkgver}"
  local kernver="$(<version)"
  local _builddir="${pkgdir}/usr/lib/modules/${kernver}/build"

  echo -e "\n${TB}* INSTALLING HEADERS${TN}"
  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers System.map version vmlinux localversion.* || exit 32
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile
  install -Dt "${_builddir}/arch/x86" -m644 "arch/x86/Makefile"
  #mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  # add objtool for external module building and enabled VALIDATION_STACK option
  install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool

  # add turbostat
  # install -Dt "${_builddir}/tools/turbostat" tools/power/x86/turbostat/turbostat

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  cp -t "${_builddir}/arch/x86" -a "arch/x86/include"
  install -Dt "${_builddir}/arch/x86/kernel" -m644 "arch/x86/kernel/asm-offsets.s"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # remove unneeded stuff
  echo -e "\n${TB}* REMOVING UNNEEDED FILES${TN}"
  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */x86/ ]] && continue
    rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # remove broken symlinks
  find -L "${_builddir}" -type l -printf 'Removing %P\n' -delete

  # remove loose objects"
  find "${_builddir}" -type f -name '*.o' -printf 'Removing %P\n' -delete

  # strip scripts directory
  echo -e "\n${TB}* STRIPPING${TN}"
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "${_builddir}" -type f -perm -u+x ! -name vmlinux -print0)
  
  strip -v $STRIP_STATIC "${_builddir}/vmlinux"
  
  echo -e "\n${TB}* SYMLINKING${TN}"
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$_builddir" "$pkgdir/usr/src/$pkgbase"
}
